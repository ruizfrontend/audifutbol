<article id="post-<?php the_ID(); ?>" <?php post_class(array('clearfix','clr-b')); ?>><div class="wrap">

	<?php the_post_thumbnail('featured-full'); ?>

  <div id="post-footer" class="clearfix wrap container">
    <div class="row">
      <p><?php echo get_avatar(get_the_author_meta( 'ID' ), 32); ?> <?php the_author(); ?> &bull; <?php the_time(get_option('date_format')); ?></p>
    </div>
  </div>
  
	<div id="post-content" class="post-content wrapper clearfix">

    <?php /*if (is_home()) : ?><span id="post-comment-total"><?php comments_number('0', '1', '%'); ?></span><?php endif; */?>

    <?php the_title('<h1 id="post-title" class="wrap">', '</h1>'); ?>
    
    <div class="social-wrap">
      <div class="social">
        <a class="social-fbook" target="_blank" href="http://www.facebook.com/sharer.php?u=http://deudafutbol.es/#<?php the_ID(); ?>" target="_blank"></a>
        <a class="social-twitter" target="_blank" href="https://twitter.com/intent/tweet?button_hashtag=deudafutbol&amp;via=deudafutbol&amp;text=<?php the_title(); ?> http://deudafutbol.es/#<?php the_ID(); ?>"></a>
      </div>
    </div>

		<?php the_content() ?>
	</div>
	<?php wp_link_pages('before=<div id="post-links">&after=</div>'); ?>
</div></article>