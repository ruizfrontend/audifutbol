<?php get_header(); ?>
<div id="post-holder" class="wrap">
	<?php
		while (have_posts()) : the_post();
			get_template_part('content', get_post_format());
			comments_template();
		endwhile;
	?>
	<?php get_sidebar(); ?>	
</div>
<?php get_footer(); ?>