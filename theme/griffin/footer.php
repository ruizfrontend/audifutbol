</div>

  <?php if (is_active_sidebar('footer-left') || is_active_sidebar('footer-middle') || is_active_sidebar('footer-right')) : ?>
  <div id="footer-widgets">
    <div class="container">
      <div class="col-md-4">
        <?php dynamic_sidebar('footer-left'); ?>
      </div>
      <div class="col-md-4">
        <?php dynamic_sidebar('footer-middle'); ?>
      </div>
      <div class="col-md-4">
        <?php dynamic_sidebar('footer-right'); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <div id="main-campaign-widget" style="display: none;">
    <div id="main-campaign-widget-close">X</div>
    <div class="cmpg-wdgt"></div>
    <div class="cmpg-wdgt-bott">
      <p>Mantente al tanto <a href="https://twitter.com/desmanesf" class="tw-inline" title="Conoce las últimas actualizaciones en twitter" target="_blank">Síguenose en Twitter</a></p>
    </div>
  </div>

<?php wp_footer(); ?>

<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script>
  'use strict';

  // Grafica VALENCIA
var wgt = {
  
  $wrap: $('#main-campaign-widget'),
  $inn: $('#main-campaign-widget .cmpg-wdgt'),
  tplElm: '<div class="cmpg-wdgt-club"><img>',
  tplLink1: '<p>El <span class="club"></span> no tiene ninguna deuda con el estado: </p><a href="http://desmanesdelfutbol.com/participa/" class="btn wk-col-r">Agradece al club</a>',
  tplLink2: '<p>El <span class="club"></span> tiene una deuda de <span class="deuda"></span>€: </p><a href="http://desmanesdelfutbol.com/participa/" class="btn wk-col-r">Presiona tu club!</a>',
  tplLink3: '<p>El <span class="club"></span> no nos dió acceso a información de su deuda: </p><a href="http://desmanesdelfutbol.com/participa/" class="btn wk-col-r">Reclama transparencia a tu club</a>',
  time: 8000,
// {{ item['fullName'] }}
// {{ item['Deuda 2014 total'] | number_format }}
  init: function() {
    
    $.getJSON('http://desmanesdelfutbol.com/data/downs/deuda.json', function(data){

      wgt.loadData(data);
      wgt.timeout();
      setTimeout(wgt.timeout, wgt.time);
      wgt.$wrap.slideDown();

      $('#main-campaign-widget-close').click(function(){
        $('#main-campaign-widget').toggleClass('closed');
        $('.cmpg-wdgt').fadeToggle();
        return false;
      });
    });
  },


  // load the data on click
  timeout: function(){
    wgt.$wrap.find('.act').removeClass('act').fadeOut(800);
    wgt.$wrap.find('.cmpg-wdgt-club').eq(Math.floor(Math.random() * wgt.$wrap.find('.cmpg-wdgt-club').length)).addClass('act').fadeIn(800);
    setTimeout(wgt.timeout, wgt.time);
  },

  // load the data on click
  loadData: function(data){
    for (var i = data.length - 1; i >= 0; i--) {
      if(data[i][1].length){
        var $elm = $(wgt.tplElm);
        $elm.find('img').attr('src', 'http://desmanesdelfutbol.com/img/equipos/' + data[i][22].replace('.png', '.jpg'));
        if((data[i][3] == 'Completo' || data[i][3] == 'COMPLETO') && data[i][6] == '0') { $elm.append(wgt.tplLink1); }
        else if(data[i][3] == 'Completo' || data[i][3] == 'COMPLETO') { 
          $elm.append(wgt.tplLink2);
          $elm.find('.deuda').html(data[i][6]); }
        else { $elm.append(wgt.tplLink3); }
        $elm.find('.club').html(data[i][19]);

        if(data[i][19] != 'Fútbol Club Barcelona B') wgt.$inn.append($elm);
      }
    };
    wgt.$wrap.find('.cmpg-wdgt-club').hide();
  },
};


'use strict';

// Objeto principal 
// -----------------------------------------------------------------------------------

var app = {
  init: function() {

    wgt.init();

  }
};


// Almacenamiento en general
// -----------------------------------------------------------------------------------
app.cache = {
  $window: $(window),

  scrollInitiated: false,

  winWidth: 0,                            // tamaños de cosas
  winHeigth: 0,

  responsive: true,
  mini: true
};


// DOCUMENT READY
// ----------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------

$( document ).ready(function() {

  app.init();

  $('.togglesubm').click(function(){ $('.subm').stop(false,false).slideToggle(); $(this).parent().toggleClass('fashion-act'); return false; })
  
});

// http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
// shim layer with setTimeout fallback
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();






</script>
</body>
</html>