<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <title>La deuda pública del fútbol español</title>

  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />    

  <meta charset="<?php bloginfo('charset'); ?>" />
  <link rel="profile" href="http://gmpg.org/xfn/11" />        
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />    

  <link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="/icons/favicon-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="/icons/favicon-160x160.png" sizes="160x160">
  <link rel="icon" type="image/png" href="/icons/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="/icons/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="/icons/favicon-32x32.png" sizes="32x32">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="/mstile-144x144.png">
  <link rel="shortcut icon" href="/icons/favicon.ico" />
    
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-58666195-1', 'auto');
    ga('send', 'pageview');
  </script>
  
  <?php wp_head(); ?>

  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
  
  <link href='http://fonts.googleapis.com/css?family=Lato:900,100|Droid+Serif:400,700italic|Merriweather:400,300,700|Old+Standard+TT:400,700,400italic' rel='stylesheet' type='text/css'>

  <?php wp_head(); ?>
  
</head>
<body <?php body_class(); ?>>

  <nav id="fbManu">
    <div id="fbManu-band">
      <div class="nav wrap">

        <a href="http://desmanesdelfutbol.com/blog/" class="wk-col-l ini">Los desmanes del fútbol</a>

        <a href="https://twitter.com/search?f=realtime&q=DesmanesF%C3%BAtbol&src=typd" target="_blank" class="wk-col-r">#DesmanesFútbol</a>
        <!-- <a href="http://desmanesdelfutbol.com/blog/?tag=nuevo" class="wk-col-r">Link a tag</a> -->
      </div>
      <!-- 
      <div class="wrap subm-wrap">
        <ul class="subm">
          <li><a href="http://desmanesdelfutbol.com/el-futbol-en-la-politica/" class="fashion">En el parlamento</a></li>
          <li><a href="http://desmanesdelfutbol.com/deuda-publica/" class="fashion">Deuda Pública</a></li>
          <li><a href="http://desmanesdelfutbol.com/futbol-de-hormigon/" class="fashion">Las obras</a></li>
          <li><a href="http://desmanesdelfutbol.com/enfoque-comunidad-valenciana/" class="fashion">C. Valenciana</a></li>
          <li><a href="http://desmanesdelfutbol.com/peticion-transparencia-futbol/" class="fashion">Campaña</a></li>
          <li><a href="http://desmanesdelfutbol.com/acerca-de/" class="fashion">Acerca de</a></li>
        </ul>
      </div> -->

    </div>

    <div id="youtubeWrapout">
      <div id="youtubeWrap">
        <iframe src="//www.youtube.com/embed/YfP9MAFG4pM?rel=0&amp;controls=0&amp;autoplay=1&amp;loop=1&amp;playlist=YfP9MAFG4pM" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </nav>

  <div id="content-holder">
