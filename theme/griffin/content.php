<article <?php post_class(array('clearfix','clr-b')); ?>><div class="wrap clearfix">
  
  <div class="post-content clearfix">

    <?php $post_tags = get_the_tags(); ?>
    <div class="post-tags">
      <?php if(isset($post_tags) && is_array($post_tags)): ?>
        <?php foreach($post_tags as $tag): ?>
          <a class="tagg<?php echo $tag->name == 'Nuevo' ? 'tag-new' : '' ?>" href="<?php echo get_tag_link($tag->term_id) ?>"><?php echo $tag->name ?></a>
        <?php endforeach; ?>
      <?php endif; ?>
      <span class="date"><?php the_time('j \d\e F, Y'); ?></span>
    </div>

    <?php the_title('<h1 class="post-title">', '</h1>'); ?>

    <div class="post-head">
      <span class="head-img"><?php echo get_avatar(get_the_author_meta('ID'), 20); ?></span>
      <span>por </span>
      <span class="head-author"><?php the_author_posts_link(); ?> · </span>
      <span class="head-share">
        <span>Compártelo</span>
        <a class="fb-inline" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank" title="Compártelo en Facebook">Compártelo en Facebook</a>
        <a class="tw-inline" target="_blank" href="https://twitter.com/intent/tweet?via=desmanesf&amp;text=<?php echo str_replace('#', '', strip_tags(the_title())); ?> <?php echo get_permalink(); ?> %23desmanesfútbol" title="Compártelo en twitter">Compártelo en twitter</a>
        <a class="mn-inline" target="_blank" href="http://meneame.net/submit.php?url=<?php echo get_permalink(); ?>&amp;text=<?php echo strip_tags(the_title()); ?>" title="Compártelo en twitter">Compártelo en twitter</a>
      </span>
    </div>
    
    <?php if (is_single()) : ?>

      <div class="post-body"><?php the_content() ?></div>

      <div class="post-footer clearfix">
        <div class="post-nav">
          <?php previous_post_link('<div class="post-nav-prev wk-col-l teaser-more"><p>&Larr; Entrada anterior</span></p>%link</div>'); ?>  
          <?php next_post_link('<div class="post-nav-next wk-col-r teaser-more"><p>Entrada siguiente <span>&Rarr;</span></p>%link</div>'); ?>   
        </div>
      </div>
    
    <?php else : ?>
      
      <?php the_excerpt(); ?>
    
    <?php endif; ?>

  </div>

</div></article>