<article>		
	<div id="post-content" class="post-content post-none clearfix wrap">
	<h1><?php _e('No encontrado','griffin'); ?></h1>
		<?php if (is_home() && current_user_can('publish_posts')) : ?>
			<p><?php printf( __('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'griffin'), admin_url('post-new.php')); ?></p>
		<?php elseif (is_search()) : ?>
			<h5><?php _e('Minuto y resultados para:', 'griffin'); echo ' "' . get_search_query() . '"'; ?></h5>
			<p><?php _e('Lo siento, su búsqueda no produjo ningún resultado. <br/>Intente otra búsqueda.', 'griffin'); ?></p>
			<?php get_search_form(); ?>
		<?php else : ?>
			<p><?php _e('Lo siento, no encontramos el artículo que buscas.', 'griffin'); ?></p>
			<?php get_search_form(); ?>
		<?php endif; ?>
	</div>
</article>