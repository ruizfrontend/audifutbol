'use strict';

  // Grafica VALENCIA
var wgt = {
  
  $wrap: $('#main-campaign-widget'),
  $elms: $('#main-campaign-widget .cmpg-wdgt-club'),
  time: 8000,

  init: function() {
    wgt.$elms.hide();
    wgt.timeout();
    setTimeout(wgt.timeout, wgt.time);
    setTimeout(function(){ wgt.$wrap.slideDown(); }, 0);
  },


  // load the data on click
  timeout: function(){
    wgt.$wrap.find('.act').removeClass('act').fadeOut(800);
    wgt.$elms.eq(Math.floor(Math.random() * wgt.$elms.length)).addClass('act').fadeIn(800);
    setTimeout(wgt.timeout, wgt.time);
  },
};
