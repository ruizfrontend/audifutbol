
/*   La deuda pública del fútbol español
Desarrollo: David Ruiz @2015__________________________ */

'use strict';
/*global labTools */
/*global Modernizr */
/*global $ */

// Objeto principal 
// -----------------------------------------------------------------------------------

var app = {
  init: function() {
    // gestión de urls____________________________________________________________________
    if (labTools.url) {
      labTools.url.initiate(app.handleUrlOnReady, app.handleUrlOnChange);
    }

      // redimensionados____________________________________________________________
    $(window).bind('orientationchange resize', app.handleResize).trigger('resize');

      // eventos scroll ____________________________________________________________
    $('body, html').add(document).add(window).bind('scroll', app.scrollHandler);//.trigger('scroll');

    app.initUI();

      // animación video
    app.youtubeAnime();


    setTimeout(function(){
        //inicio gráficos

      ftblGraph2.init();
      ftblMap.init();
      //ftblGraph1.init();
      wgt.init();

        // init timeline
      $('#timeline').attr('src', 'http://cdn.knightlab.com/libs/timeline/latest/embed/index.html?source=0AnAKfyGY_14jdHVucjl2ZkcyUlAxNlBkVWoyYUpsZnc&font=Georgia-Helvetica&maptype=toner&lang=es&height=650');
    }, 0);

  },
          // función control de redimensionados______________________________________________________
  handleResize: function() {
    app.cache.winWidth = app.cache.$window.width();
    app.cache.winHeigth = app.cache.$window.height();

    app.cache.responsive = app.cache.winWidth < 800 ? true : false;
    app.cache.mini = app.cache.winWidth < 600 ? true : false;

    var sects = $('.bl-sct');
    app.cache.sctsTop = [{'url': '/', 'top': sects.eq(0).offset().top }];
    $('.bl-sct').each(function(){
      var $this = $(this);
      app.cache.sctsTop.push({'url': $this.data('url'), 'top':parseInt($this.offset().top + $this.height(), 10)});
    });

      // redimensiono gráficos
    if(ftblMap) ftblMap.handleResize();

    // mi_modulo.handleResize()
    //labTools.console.log('resize');
  },
          // control de scroll______________________________________________________
  scrollHandler: function(e) {
    if(!app.cache.scrollInitiated) return;
    // mi_modulo.scrollHandler()
    var scroll = app.cache.$window.scrollTop();
    for (var i = 0; i <= app.cache.sctsTop.length; i++) {
      if(scroll < app.cache.sctsTop[i].top) {
        if(app.cache.activeUrl != app.cache.sctsTop[i].url) {
          app.cache.activeUrl = app.cache.sctsTop[i].url;
          app.goUrl(app.cache.activeUrl.substring(1));
        }
        return;
      }
    }
    app.goUrl('');
  },
          // función control de las urls______________________________________________________
  handleUrlOnChange: function(url) {
    labTools.console.log('change', url);
  },
  handleUrlOnReady: function(url) {
    labTools.console.log('ready', url);
    var target = location.pathname.substr(labTools.url.data.baseUrl.toString().length-1);
    var $target = $('[data-url="' + target + '"]').offset();
    if($target && target) app.cache.$window.scrollTop($target.top);
    app.goUrl(target);

    setTimeout(function(){ app.cache.scrollInitiated = true;}, 100);
  },

  goUrl: function(url) {
    if(url == '/') url = '';
    if(app.cache.scrollInitiated) labTools.url.setUrl(url); // evito la primera ejecución
    
    $('#fbManu .act').removeClass('act');

    if(!url) $('#fbManu li:eq(0)').addClass('act');
    
    var target = $('[data-url="/' + url + '"]').attr('id');
    var url = target ? target.substr(4) : '';

    $('[href="#sct-' + url + '"]').parents('li').first().addClass('act');
  },

  initUI: function() {

      // CAMPAÑA ADMINISTRACIONES
    $('.cmpg-club-row').mouseenter(function(){
      var $form = $('#form-twt');
      var $form2 = $('#form-twt2');
      var $this = $(this);
      
      if($(this).parents('.cmpg-list-ok-2').length) {
        $form.find('#twtxt-team, #twtxt-team2').html('@' + $this.data('tw')).end();
        $form2.find('#twtxt-team, #twtxt-team2').html('@' + $this.data('tw')).end();
      } else if($(this).parents('.cmpg-list-ok-1').length) {
        $form.find('#twtxt-team, #twtxt-team2').html('@' + $this.data('tw')).end();
        $form2.find('#twtxt-team, #twtxt-team2').html('@' + $this.data('tw')).end();
      } else {
        $form
          .find('#twtxt-team').html('@' + $this.data('tw')).end()
          .find('#twtxt-frase').html(' por ser un equipo libre de deuda').end()
          .find('#twtxt-frease2').removeClass('visible').hide();
      }
    });

    $('.cmpg-club-row').click(function(){
      $('.footer-twitter').click();
    });
    
      // CAMPAÑA CLUBES
    $('.cmpg-admin-row').mouseenter(function(){
      var $form2 = $('#form-twt2');
      var $this = $(this);
      
      $('#frm-ss').html('@' + $this.data('tw'));
    });

    $('.cmpg-admin-row').click(function(){
      $('.footer-twitter2').click();
    });


    if(document.cookie.indexOf('desmanesdelfutbol') != -1){
      $('#politcs').hide();
    } else {
      document.cookie="desmanesdelfutbol=si";
    }


    $('#closePolitics').click(function(){
      $('#politcs').slideUp();
      return false;
    });

    $('#showCookies').click(function(){
      $('#cookies').slideDown();
      return false;
    });

    $('.footer-twitter').click(function(){
      var $form = $('#formInn').clone();
      if(!$('#formInn').find('#twtxt-frase3').hasClass('visible')) $form.find('#twtxt-frase3').remove();
      if(!$('#formInn').find('#twtxt-frease2').hasClass('visible')) $form.find('#twtxt-frease2').remove();
      var opt = $('#formInn').find('option:selected').text();
      $form.find('select').html(opt);
      var text = $form.text();
      var link = 'https://twitter.com/intent/tweet?button_hashtag=DesmanesFútbol&amp;via=desmanesf&amp;text=' + text.trim();
      window.open(link);
      $(this).attr('href', link);
      //return false;
      // https://twitter.com/intent/tweet?button_hashtag=deudafutbol&amp;via=deudafutbol&amp;text=Solicito a @deportegob que publique la deuda de cada equipo de @LaLiga por un deporte transparente #desmanesdelfutbol
    });

    $('.footer-twitter2').click(function(){
      var $form = $('#formInn2').clone();
      $form.find('#frm-kind2').html($('#frm-kind2').find('option:selected').text());
      var text = $form.text();
      var link = 'https://twitter.com/intent/tweet?button_hashtag=DesmanesFútbol&amp;via=desmanesf&amp;text=' + text.trim();
      window.open(link);
      $(this).attr('href', link);
      //return false;
      // https://twitter.com/intent/tweet?button_hashtag=deudafutbol&amp;via=deudafutbol&amp;text=Solicito a @deportegob que publique la deuda de cada equipo de @LaLiga por un deporte transparente #desmanesdelfutbol
    });

    $('#sendForm').click(function(){
      var mail = $('#frm-mail').val();
      var usr = $('#frm-name').val();
      var dni = $('#frm-num').val();
      if(!mail || !usr || !dni || !/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(mail)) {
        $('#errorForm').addClass('error').slideDown().html('Rellene correctamente todos los campos de la solicitud');
        return false;
      }

      $.ajax('./data/poll.php', {
        data: {
          usr: usr,
          mail: mail,
          dni: dni
        }
      })
      .done(function(data){
        $('#errorForm').removeClass('error').slideDown().html('Solicitud enviada correctamente, gracias.');
      })
      .error(function(data, e ,f){
        $('#errorForm').addClass('error').slideDown().html('Se produjo un error enviando los datos: ' + data.responseJSON.error || '');
      });

      return false;
    });

    $('#main-campaign-widget-close').click(function(){
      $('#main-campaign-widget').toggleClass('closed');
      $('.cmpg-wdgt').fadeToggle();
      return false;
    });

      // interacciones generales
    $('#closeMenu').click(function(){
      $('#fbManu ul').slideToggle();
      return false;
    });

    $('#fbManu a, a.goto').click(function(){
      var $this = $(this);
      if($this.parents('.disabled').length) return false;
      var $footer = $('footer');
      var $act = $footer.find('.ft-block.active');
      
        // sobre el equipo
      if($this.attr('id') == 'closeMenu') return false;

      $('html, body').animate({ scrollTop: $($this.attr('href')).offset().top }, 400);
      return false;
    });

    $('#infoForm').click(function(){
      $('#form-more').slideToggle();
      return false;
    })

    $('#main-campaign-widget .cmpg-wdgt-club').click(function(){
      $('html, body').animate({ scrollTop: $('#sct-05-campaign').offset().top }, 400);
      return false;
    });

    $('#footer-fix a').click(function(){
      var $this = $(this);
      var target = $(this).attr('id').substring(3);
      var $footer = $('footer');
      var $act = $footer.find('.ft-block.active');

      if($act.attr('id') == 'footer-' + target) {
        $this.removeClass('active');
        $act.removeClass('active').slideUp();
        return false;
      }

      $('#footer-fix .active').removeClass('active');
      $act.removeClass('active').slideUp();
      $('#footer-' + target).addClass('active').slideDown();
      $this.addClass('active');

      return false;
    });

  },

  youtubeAnime: function() {

    $('#headWrap .head-block').clearQueue().hide();
    $('#headWrap .head1').delay(2100).fadeIn(400, function(){ $(this).addClass('fashion-act'); }).delay(5000).fadeOut(400, function(){ $(this).removeClass('fashion-act'); });
    $('#headWrap .head2').delay(10000).fadeIn(400, function(){ $(this).addClass('fashion-act'); }).delay(5000).fadeOut(400, function(){ $(this).removeClass('fashion-act'); });
    $('#headWrap .head3').delay(20000).fadeIn(400, function(){ $(this).addClass('fashion-act'); }).delay(7000).fadeOut(400, function(){ $(this).removeClass('fashion-act'); });
    $('#headWrap .head4').delay(30000).fadeIn(400, function(){ $(this).addClass('fashion-act'); }).delay(7000).fadeOut(400, function(){ $(this).removeClass('fashion-act'); });
    $('#headWrap .head5').delay(40000).fadeIn(400, function(){ $(this).addClass('fashion-act'); });

  }
};


// Almacenamiento en general
// -----------------------------------------------------------------------------------
app.cache = {
  $window: $(window),

  scrollInitiated: false,

  winWidth: 0,                            // tamaños de cosas
  winHeigth: 0,

  responsive: true,
  mini: true
};


// DOCUMENT READY
// ----------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------

$( document ).ready(function() {

  app.init();

});

// http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
// shim layer with setTimeout fallback
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();





