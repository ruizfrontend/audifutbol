'use strict';

angular.module('valencia', [])
.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
})
.filter('toEuros', function() {
  return function(orginalVal) {
    return orginalVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + '€';
  };
})
.filter('arrayTostring', function() {
  return function(orginalVal) {
    return angular.isArray(orginalVal) ? orginalVal.join(', ') : orginalVal;
  };
})
.directive('preventDef', function() {
  return function(scope, element, attrs) {
    $(element).click(function(event) {
      event.preventDefault();
      return false;
    });
  }
})
.controller('valenciaController', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {
  $scope.data = {
    bruto: [],
    max: 0,
    filtros: {
      tipos: [],
      equipos: [],
      motivo: [],
      from: [],
      years: []
    },
    filtrosAct: {
      tipos: [],
      equipos: [],
      motivo: [],
      from: [],
      years: ['2014', '2012']
    },
    filtered: []
  };

  $scope.status = {
    equiposAct: false,
    yearsAct: true,
    fromAct: false,
    motivoAct: false,
    total: 0,
    sortBy: 'Cantidadok'
  };

  // load the data on click
  $scope.fns = {
    // data recieved => processing
    initData: function(data) {

      var dataOk = [];

      for (var i = data.length - 1; i >= 0; i--) {
        if(!data[i]['Contrato'].length || data[i]['Cantidad'] == '-') continue; // fila vacia

        dataOk.push(data[i]);

        data[i]['Cantidadok'] = parseInt(data[i]['Cantidad'].replace(/,/g, ''));
        data[i]['yearIni'] = data[i]['Año'].split('/')[0];
        data[i]['yearEnd'] = data[i]['Año'].split('/')[1];

        if($scope.data.max < parseInt(data[i]['Cantidadok'])) $scope.data.max = data[i]['Cantidadok'];

        if($scope.data.filtros.from.indexOf(data[i]['De']) == -1) $scope.data.filtros.from.push(data[i]['De']);
        if($scope.data.filtros.equipos.indexOf(data[i]['A']) == -1) $scope.data.filtros.equipos.push(data[i]['A']);
        if($scope.data.filtros.years.indexOf(data[i]['yearIni']) == -1) $scope.data.filtros.years.push(data[i]['yearIni']);
        if($scope.data.filtros.years.indexOf(data[i]['yearEnd']) == -1) $scope.data.filtros.years.push(data[i]['yearEnd']);
        if($scope.data.filtros.motivo.indexOf(data[i]['Motivo']) == -1) $scope.data.filtros.motivo.push(data[i]['Motivo']);

      }

      $scope.data.bruto = dataOk;

          // set Watch
      $scope.$watch('data.filtrosAct', $scope.fns.updateFilters, true);

    },
    updateFilters: function() {
      var results = [];

      for (var i = $scope.data.bruto.length - 1; i >= 0; i--) {
        if($scope.data.filtrosAct.from.length && $scope.data.filtrosAct.from.indexOf($scope.data.bruto[i]['De']) == -1) continue;
        if($scope.data.filtrosAct.equipos.length && $scope.data.filtrosAct.equipos.indexOf($scope.data.bruto[i]['A']) == -1) continue;
        if($scope.data.filtrosAct.motivo.length && $scope.data.filtrosAct.motivo.indexOf($scope.data.bruto[i]['Motivo']) == -1) continue;
        if($scope.data.filtrosAct.years.length){
          var foundYear = false;
          for (var j = $scope.data.bruto[i]['yearIni']; j <= $scope.data.bruto[i]['yearEnd']; j++) {
            if($scope.data.filtrosAct.years.indexOf(j.toString()) != -1) foundYear = true;
          }
          if(!foundYear) continue;
        }
        results.push($scope.data.bruto[i]);
      }

        // calculos intermedios
      $scope.status.total = 0
      var max = 0, coef = 50;
      for (var i = results.length - 1; i >= 0; i--) {
        $scope.status.total += results[i]['Cantidadok'];
        if(results[i]['Cantidadok'] > max) max = results[i]['Cantidadok'];
      }
      for (var i = results.length - 1; i >= 0; i--) {
        results[i]['width'] = coef * results[i]['Cantidadok'] / max;
      }
      $scope.data.filtered = results;
    }
  };
console.log(proyData['valencia'])
  $scope.fns.initData(proyData['valencia']);
}]);