'use strict';

  // MAPA
var ftblMap = {
    // caches
  map: null,
  map2: null,
  center: new google.maps.LatLng(39.6,-3.7),
  center2: new google.maps.LatLng(28,-15.7),

  slow: 900,
  fast: 500,
  ratio: 9000000,
  minHeight: 60,
  timeout: [],
  activeYear: 2014,

  isplaying: false,

  $mapWrap: $('#map-pointers-wrap'),
  $mapMain: $('#map-main'),

  init: function() {

    if(app.cache.responsive){
      ftblMap.$mapWrap.remove();
      $('#ftbl-table table, #ftbl-table p').hide();
      $('#datosOut').show();

      $('.toggleMapTable').click(function(){
        $('#ftbl-table, #ftbl-table table').slideToggle();
      });
      return;
    }

    $('.toggleMapTable').click(function(){
      $('#ftbl-table, #ftbl-info').slideToggle();
    });

    $('.showInfo').click(function(){
      ftblMap.popInfo();
    });

    ftblMap.$mapWrap = $('#map-pointers-wrap');
    ftblMap.$mapMain = $('#map-main');

    $('#ftbl-table, #ftbl-info').hide();
    
    ftblMap.$mapMain.show()
      .delegate('.obra-concepto, .obra-gasto', 'click', ftblMap.drawPop);

    $('#map-wrap').delegate('#map-pop-close', 'click', function(){
      $(this).parents('#map-pop').fadeOut(400);
    });


    // UI interactions
    ftblMap.initData();


  },
  processLinks: function(links){
    var count = 1;
    for (var i = links.length - 1; i >= 0; i--) {
      if(links[i].search('wikipedia') != -1) {
        links[i] = '<a href="' + links[i] + '" target="_blank" class="link wikipedia">' + decodeURI(links[i]) + '</a>';
      } else {
        links[i] = '<a href="' + links[i] + '" target="_blank" class="link">' + decodeURI(links[i]) + '</a>';
      }
    }

    return '<div class="pop-team">Fuentes</div>' + links.join('');
  },

  // load the data on click
  initData: function(){

    // XHR in action
    ftblMap.processData(proyData['obras']);

  },

  handleResize: function() {
    if(ftblMap.map) setTimeout( function() { ftblMap.map.panTo( ftblMap.center ); }, 200);
  },

  // data recieved => processing
  processData: function(data) {

    // initiate the map
    if(!ftblMap.map) ftblMap.initiateMap();


    for (var i = data.length - 1; i >= 0; i--) {
      var found = false;
      switch (data[i].equipo) {
        case 'Real Jaén': data[i].img = 'jaen.jpg'; break;
        case 'Villareal': data[i].img = 'villareal.jpg'; break;
        case 'Real Murcia': data[i].img = 'murcia.jpg'; break;
        case 'Éibar': data[i].img = 'eibar.jpg'; break;
        case 'Sporting': data[i].img = 'molinon.jpg'; break;
        default:
          for (var j = proyData.deuda.length - 1; j >= 0; j--) {
            if(proyData.deuda[j].simpleName == data[i].equipo) {
              data[i].img = proyData.deuda[j].foto;
              found = true;
              break;
            }
          };
        break;
      }
    };

    ftblMap.data = data;
    ftblMap.max = ftblMap.getMax();

    for (var i = 2014; i >= 2000; i--) {
      $('#years').append('<a href="#" class="btn btn-default year-' + i + '" data-year="' + i + '">' + i + '</div>');
    }
    $('#years').delegate('a', 'click', function(){
      if(ftblMap.isplaying) return false;
      ftblMap.activeYear = $(this).data('year');
      ftblMap.activaYear(ftblMap.activeYear);
      return false;
    });

    $('#animaYears').click(function(){
      if(ftblMap.isplaying) {
        ftblMap.stopAnima();
        ftblMap.isplaying = false;
        $('#animaYears').removeClass('active').text('play');
      } else {
        ftblMap.playYear();
        ftblMap.isplaying = true;
        $('#animaYears').addClass('active').text('pausa');
      }
      return false;
    });

    ftblMap.activaYear(2014);

  },

  stopAnima: function(){console.log('stop')
    for (var i = ftblMap.timeout.length - 1; i >= 0; i--) {
      clearTimeout(ftblMap.timeout[i]);
    }
    ftblMap.timeout = [];
  },

  getMax: function() {
    var max = 0, gasto;

    for (var i = ftblMap.data.length - 1; i >= 0; i--) {
      ftblMap.data[i].gastoOk = parseInt(ftblMap.data[i].gasto.replace(/[^0-9\.]+/g,""), 10) || 0;
      if(max < ftblMap.data[i].gastoOk) max = ftblMap.data[i].gastoOk;
    }

    return max;
  },

  playYear: function() {

    ftblMap.clearObras();

    var idx = 0;

    if(ftblMap.activeYear == 2014) ftblMap.activeYear = 1999;

    for (var i = ftblMap.activeYear + 1; i <= 2014; i++) {
      idx++;
      ftblMap.timeoutYear(idx, i);
    }
  },

  getObraidx: function(idx){
    for (var i = ftblMap.data.length - 1; i >= 0; i--) {
      if(ftblMap.data[i]['index'] == idx) return ftblMap.data[i];
    };
    return false;
  },

  timeoutYear: function(i, actYear){

    var tiempoAparición = (100 * i) + ftblMap.fast * (i-1) * 4; // TIEMPO ENTRE ANIMACIONES
    ftblMap.timeout.push(setTimeout( function(){
      ftblMap.activeYear = actYear;
      if(ftblMap.timeout.length) {
        ftblMap.timeout.shift();
      } else {
        ftblMap.isplaying = false;
        $('#animaYears').removeClass('active').text('play');
      }
      if(actYear == 2014) {
        ftblMap.isplaying = false;
        $('#animaYears').removeClass('active').text('play');
      }
      ftblMap.activaYear(actYear, 'fast');
    }, tiempoAparición));
    
  },

  activaYear: function(year, speed) {

    // pinta pager
    $('#years')
    .find('.active').removeClass('active').end()
    .find('.year-' + year).addClass('active');

    $('.tpl-year').html(year);

    var total = 0;
    var elms = 0;

    // recorre datos y pinta o borra
    for (var i = ftblMap.data.length - 1; i >= 0; i--) {
      if((ftblMap.data[i]['planificado'] || ftblMap.data[i]['inicio']) <= year && ftblMap.data[i].final >= year){
        total += parseInt(ftblMap.data[i]['gastoOk'],10);
        elms ++;
        ftblMap.drawObra(ftblMap.data[i], speed, i);
      } else {
        ftblMap.clearObra(i);
      }
    }
    $('#numYear').html(elms);
    $('#totYear').html((total / 1000000).toString().replace('.', ',') + 'm€' );
  },

  // borra todo
  clearObras: function() {
    $('.obra').addClass('obraremove').fadeOut(100);
    setTimeout(function(){ $('.obraremove').remove(); }, 100);
  },

  // borra una obra
  clearObra: function(i) {
    $('.obra-' + i).addClass('obraremove').fadeOut(100);
    setTimeout(function(){ $('.obraremove').remove(); }, 100);
  },

  // pinta una obra
  drawObra: function(data, speed, idx) {
    // si ya está pimtada, nos vamos
    if(ftblMap.$mapMain.find('.obra-' + idx).length) return;

    speed = speed || 'slow';

    var hgt = ftblMap.minHeight + (data.gastoOk / ftblMap.ratio);
    if(idx == 10 || idx == 8 || idx == 9) hgt *= 1.3;
    if(idx == 9) hgt *= 1.3;

    var bottom = [17,14,7,9,4,18,33];

    var $pop = $('<div style="height: 0" data-obra="' + idx + '" class="obra obra-' + idx + '" style="">' +
      //'<div class="obra-name" style=" opacity: 0; " >' + data.equipo + '</div>' +
      '<div class="obra-concepto" style=" opacity: 0; " >' + data.concepto +
        '<div class="cmpg-club-hid' + (bottom.indexOf(idx) != -1 ? ' cmpg-club-bottom' : '') + '">' +
          '<h3>' + data['equipo'] + '<br/>' +
          (data['planificado'] ? data['planificado'] : data['inicio']) + ' - ' + (data['final'] && data['final'] != 3000 ? data['final'] : '') +
          '<br/>' + data['gasto'] +
          '<br/><small>Haz click para ver más detalles</small>' +
          '</h3>' +
        '</div>' +
      '</div>' +
      //'<div class="obra-gasto" style=" opacity: 0; " >' + ((data.gastoOk / 1000000) || '-') + 'm€</div>' +
      '</div>');

    if(idx == 31) {
      $('#map-2').append($pop);
    } else {
      ftblMap.$mapWrap.append($pop);
    }

    ftblMap.$mapMain
      .find('.obra-' + idx)
      .animate({ height: hgt}, 2 * ftblMap[speed])
      .find('.obra-concepto')
      .delay(ftblMap[speed] / 2).animate({ opacity: 1}, ftblMap[speed]).end()
      .find('.obra-gasto')
      .delay(ftblMap[speed]).animate({ opacity: 1 }, ftblMap[speed]).end();

  },

  drawPop: function() {
    if(ftblMap.isplaying) {
      ftblMap.stopAnima();
      ftblMap.isplaying = false;
      $('#animaYears').removeClass('active').text('play');
    }

    var $this = $(this).parents('.obra');
    var data = ftblMap.data[$this.data('obra')];

    ftblMap.drawPopData(data);
  },

  drawPopData: function(data) {
    var $wrap = $('#map-pop .wrap-pop');

    $wrap
      .html('')
      .append('<img class="foto" src="' + labTools.url.data.baseUrl + 'img/obras/' + 'obra-' + data['index'] + '.jpg">');

    var $main = $('<div class="pop-half"><img class="foto-team" src="./img/equipos/' + data['img'].replace('.png', '.jpg') + '">')
      .append('<h2>' + data['concepto'] + '</h2>' +
        '<p class="pop-team">' + data['equipo'] + '<ul>');

    if(data['comments']) $main.append('<p class="comments">' + data['comments'] + '</p>');

    var li = '<li>Estado: ' + data['estado'] + '</li>';
    if(data['planificado']) {
      li += '<li class="comiento year">Planeación: <span>' + data['planificado'] + '</span></li>';
    } else {
      li += '<li class="comiento year">Comienzo de obras: <span>' + data['inicio'] + '</span></li>';
    }
    if(data['final'] && data['final'] != 3000) li += '<li class="comiento year">Final de obras: <span>' + data['final'] + '</span></li>';
    if(data['gasto']) li += '<li class="comiento year">Presupuesto estimado: <span><strong>' + data['gasto'].replace(new RegExp('[.]', 'g'), '-').replace(new RegExp('[,]', 'g'), '.').replace(new RegExp('[-]', 'g'), ',') + '<strong></span></li>';

    $main
      .find('ul').append(li);

    $main.append(ftblMap.processLinks(data['links'].split(' ')));

    $wrap
      .append($main)
      .append('<div class="pop-quote">' + data['quote'] + '</div>')
      .parents('#map-pop').fadeIn(400);

  },

  popTable: function() {
    if(ftblMap.isplaying) {
      ftblMap.stopAnima();
      ftblMap.isplaying = false;
      $('#animaYears').removeClass('active').text('play');
    }

    $('#map-pop .wrap-pop')
      .html('')
      .append($('#ftbl-table').html())
      .parents('#map-pop').fadeIn(400)
      .find('table')
        .delegate('tr', 'click', function(){
          var idx = $(this).data('idx');
          if(!idx) return;
          ftblMap.drawPopData(ftblMap.data[idx]);console.log(ftblMap.data[idx]);
        })
        .tablesorter({ sortList: [[4,1]]});

    return false;
  },

  popInfo: function() {
    if(ftblMap.isplaying) {
      ftblMap.stopAnima();
      ftblMap.isplaying = false;
      $('#animaYears').removeClass('active').text('play');
    }

    $('#map-pop .wrap-pop')
      .html('')
      .append($('#ftbl-about').html())
      .parents('#map-pop').fadeIn(400);

    return false;
  },

  initiateMap: function() {

    // initial settings for the map
    var mapOptions = {
      zoom: 6,
      center: ftblMap.center,
      disableDefaultUI: true,
      disableDoubleClickZoom: true,
      draggable: false,
      scrollwheel: false,
      panControl: false,
      styles: [
      {
        "featureType": "road",
        "stylers": [
        { "visibility": "off" }
        ]
      },{
        "featureType": "water",
        "stylers": [
        { "visibility": "simplified" },
        { "hue": "#0008ff" },
        { "saturation": -50 }
        ]
      },{
        "featureType": "poi",
        "stylers": [
        { "visibility": "off" }
        ]
      },{
        "featureType": "landscape",
        "stylers": [
        { "visibility": "on" },
        { "hue": "#ff0022" },
        { "saturation": -83 }
        ]
      },{
        "elementType": "labels",
        "stylers": [
        { "visibility": "off" }
        ]
      }
      ]
    };

    // draw the map
    ftblMap.map = new google.maps.Map($('.ftbl-map-inn')[0], mapOptions);
    mapOptions.center = ftblMap.center2;
    ftblMap.map2 = new google.maps.Map($('#map-2-inn')[0], mapOptions);

  },

  // error with data => whatever
  ajaxError: function(jqXHR, status, error) {
    console.log(error);
  }
};


