

  // Grafica DEUDA
var ftblGraph2 = {
  
  dataOk: {
    d1hac: {},
    d2hac: {},
    d1ss: {},
    d2ss: {}
  },

  activeGraph: {
    d1hac: [],
    d2hac: [],
    d1ss: [],
    d2ss: []
  },

  graph: {
    labels: []
  },

  g: {
    d1hac: [],
    d2hac: [],
    d1ss: [],
    d2ss: []
  },

  temps: [1],
  dato: ['hac'],

  animated: false,

  max: 0,

  $graphWrap: $('#ftblGraph2 .graph'),

  init: function() {

      // botones
    $('#toggletable2').click(function(){
      $('.g2-tabla').css({'visibility': 'inherit'}).slideToggle();
      return false;
    });

    var years = ftblGraph2.$graphWrap.find('.g2-gside');
    for (var i = 1999; i <= 2012; i++) {
      years.append('<div class="year">' + i + '/' + (i+1) + '</div>');
    }

    ftblGraph2.$graphWrap.find('.g2-ctrls .wk-col-l a').click(function(){
      if(ftblGraph2.animated) return;

      var $this = $(this);
      var $wrap = $this.parents('.wk-col-l');

      if($this.hasClass('active')){
        if($wrap.find('.active').length == 1) return false;
        ftblGraph2.temps.splice(ftblGraph2.temps.indexOf($this.data('div')),1);
        $this.removeClass('active');
      } else {
        ftblGraph2.temps.push($this.data('div'));
        $this.addClass('active');
      }

      ftblGraph2.updateGraph();
      return false;
    });

    ftblGraph2.$graphWrap.find('.g2-ctrls .wk-col-r a').click(function(){
      if(ftblGraph2.animated) return;

      var $this = $(this);
      var $wrap = $this.parents('.wk-col-r');

      if($this.hasClass('active')){
        if($wrap.find('.active').length == 1) return false;
        ftblGraph2.dato.splice(ftblGraph2.dato.indexOf($this.data('tipe')),1);
        $this.removeClass('active');
      } else {
        ftblGraph2.dato.push($this.data('tipe'));
        $this.addClass('active');
      }

      ftblGraph2.updateGraph();
      return false;
    });

    ftblGraph2.$graphWrap.find('canvas').attr('width', $('.wrap:eq(0)').width()).attr('height', $('.wrap:eq(0)').width() * 0.6);
    // UI interactions
    ftblGraph2.processData(proyData['deudaResumen']);

  },

  updateGraph: function() {
    var max = ftblGraph2.getMax(); // VALOR MÁXIMO DE LAS SERIES ACTIVADAS
    var hght = 30;

    var graph = {
      labels: ftblGraph2.graph.labels,
      datasets: []
    };

    var color = {
      d1hac: 'rgb(63, 135, 205)',
      d2hac: 'rgb(132, 166, 199)',
      d1ss: 'rgb(189, 51, 108)',
      d2ss: 'rgb(211, 141, 170)'
    };

    for (var i = ftblGraph2.temps.length - 1; i >= 0; i--) {
      for (var j = ftblGraph2.dato.length - 1; j >= 0; j--) {

        graph.datasets.push({
          label: "My First dataset",
          fillColor: "rgba(220,220,220,0.2)",
          strokeColor: color['d' + ftblGraph2.temps[i] + ftblGraph2.dato[j]],
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: ftblGraph2.g['d' + ftblGraph2.temps[i] + ftblGraph2.dato[j]]
        });
        /*
        var serie = 'd' + ftblGraph2.temps[i] + ftblGraph2.dato[j];
        var datos = ftblGraph2.dataOk[serie];
        var path = $('#' + serie);

        var target = [];
        var count = 0;
        for(var dato in datos){
          target.push([ (hght/2) + hght * count, 100 * parseInt(datos[dato], 10) / max]);
          count += 1;
        }
        ftblGraph2.animateSerie(serie, target);*/
      }
    }
    
    // new Chart($('.g2-graph')[0].getContext("2d")).Line(graph, { bezierCurve : false });

  },

  animateSerie: function(serie, target){
    ftblGraph2.animated = true; // semaforo

    var steps = 20;
    var time = 20;
    var end = false;

    for (var i = 0; i < steps; i++) {
      if(i == steps - 1) end = true;
      ftblGraph2.timeOutAnimate(i/steps, i*time, serie, target, end);
    }
  },

  timeOutAnimate: function(percent, time, serie, target, end){
    setTimeout(function(){
      $('#'+serie).attr('d', ftblGraph2.getPath(percent, target, serie));
      if(end){
        ftblGraph2.activeGraph[serie] = target;
        ftblGraph2.animated = false; // semaforo
      }
    }, time);
  },

  getPath: function(percent, target, serie){
    
    var outp = '';
    for (var i = 0; i < target.length; i++) {
      ftblGraph2.activeGraph[serie][i] = ftblGraph2.activeGraph[serie][i] ? ftblGraph2.activeGraph[serie][i] : [0,0];
      var op = 'L';
      var Xprev = ftblGraph2.activeGraph[serie][i][1] || 0;
      var X = Xprev + (percent *(target[i][1] - Xprev));

      if(i == 0) op = 'M';
      outp += op + X + ' ' + target[i][0];
    }
    return outp;
  },

  getMax: function(){
    var max = 0, maxSerie;

    for (var i = ftblGraph2.temps.length - 1; i >= 0; i--) {
      for (var j = ftblGraph2.dato.length - 1; j >= 0; j--) {
        maxSerie = ftblGraph2.getMaxSerie(ftblGraph2.dataOk['d' + ftblGraph2.temps[i] + ftblGraph2.dato[j]]);
        max = max > maxSerie ? max : maxSerie;
      }
    }

    return max;
  },

  getMaxSerie: function(serie) {

    var max = 0;
    for (var val in serie) {
      max = max > parseInt(serie[val],10) ? max : parseInt(serie[val],10);
    }

    return max;
  },

  // data recieved => processing 
  processData: function(data) { console.log('proces')

    for (var i = 0; i < data.length; i++) {
      ftblGraph2.graph.labels.push(data[i]['Temporada']);

      ftblGraph2.dataOk.d1hac[parseInt(data[i]['Temporada'],10)] = data[i]['1ª Deuda con Hacienda'].replace(/,/g,'');
      ftblGraph2.g.d1hac.push(parseInt(data[i]['1ª Deuda con Hacienda'].replace(/,/g,'')));

      ftblGraph2.dataOk.d2hac[parseInt(data[i]['Temporada'],10)] = data[i]['1ª Deuda con Seguridad Social '].replace(/,/g,'');
      ftblGraph2.g.d2hac.push(parseInt(data[i]['1ª Deuda con Seguridad Social '].replace(/,/g,'')));

      ftblGraph2.dataOk.d1ss[parseInt(data[i]['2ª Deuda con Hacienda'],10)] = data[i]['2ª Deuda con Hacienda'].replace(/,/g,'');
      ftblGraph2.g.d1ss.push(parseInt(data[i]['2ª Deuda con Hacienda'].replace(/,/g,'')));

      ftblGraph2.dataOk.d2ss[parseInt(data[i]['Temporada'],10)] = data[i]['2ª Deuda con Seguridad Social '].replace(/,/g,'');
      ftblGraph2.g.d2ss.push(parseInt(data[i]['2ª Deuda con Seguridad Social '].replace(/,/g,'')));
    }

    ftblGraph2.updateGraph();

    return;

  },

  // error with data => whatever
  ajaxError: function(jqXHR, status, error) {
    console.log(error);
  }
};