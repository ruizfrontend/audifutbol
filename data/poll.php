<?php
setlocale(LC_ALL,"es_ES");
define('SITE_ROOT', dirname(__FILE__));
header('Content-Type: application/json; charset=utf-8');

ini_set('max_execution_time', 30000);

$statusFile = './status.json';
$totalFile = './total.json';
$hoursFolder = './hours/';

error_reporting(E_ALL);
ini_set('display_errors', 1);

  // limpia todos los datos => DANGER
if(isset($_GET['clear'])){
  clearData(); 
  echo 'datos borrados';
  die();
};

if(!isset($_GET['usr']) || !isset($_GET['mail']) || !isset($_GET['dni'])) {
  header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
  die('{ "error": "Error obteniendo parámetros"}');
}

$DATAuser = safeString($_GET['usr']);
$DATAmail = safeString($_GET['mail']);
$DATAdni = safeString($_GET['dni']);

if(strlen($DATAuser) > 300 || strlen($DATAmail) > 300 || strlen($DATAdni) > 300) {
  header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
  die('{ "error": "Parámetros demasiado largos"}');
}

initData();

array_push($hour['votos'], array('user' => $DATAuser, 'dni' => $DATAdni, 'mail' => $DATAmail));

saveData();

echo '{ "usr": "' . $DATAuser . '", "mail": "' . $_GET['mail'], '", "dni": "' . $_GET['dni'] . '"}';
return;


function initData(){
  //$GLOBALS['time'] = date("ymdH");
  $GLOBALS['time'] = date("ymdH");

    // si no hay estatus reiniciamos proyecto
  if(!file_exists($GLOBALS['statusFile'])) {
    $GLOBALS['status'] = array('time' => $GLOBALS['time'], 'procesed' => array());
  } else {
    $GLOBALS['status'] = json_decode(file_get_contents($GLOBALS['statusFile']), true);
  }

    // si no hay datos de la hora, procesamos y generamos nuevo
  $GLOBALS['hourFile'] = $GLOBALS['hoursFolder'].$GLOBALS['time'].'.json';

  if(!file_exists($GLOBALS['hourFile'])) {
    $GLOBALS['hour'] = array('votos' => array());
  } else {
    $GLOBALS['hour'] = json_decode(file_get_contents($GLOBALS['hourFile']), true);
  }

}


function saveData(){
  writeJson($GLOBALS['statusFile'], $GLOBALS['status']);
  writeJson($GLOBALS['hourFile'], $GLOBALS['hour']);
}


function clearData(){

  $hours = glob($GLOBALS['hoursFolder'].'*'); // get all file names

  foreach($hours as $file){ // iterate files
    if(is_file($file)) unlink($file); // delete file
  }

  if(file_exists($GLOBALS['statusFile'])) unlink($GLOBALS['statusFile']);
  if(file_exists($GLOBALS['totalFile'])) unlink($GLOBALS['totalFile']);
}


/* Funciones de escritra de archivos
      ---------------*/

function writeJson($fileName, $array) {
  $out = fopen($fileName, 'w');
  fwrite($out, json_encode($array));
  fclose($out);
}

function safeString($string) {
  $invalid_characters = array("$", "%", "#", "<", ">", "|");
  $string = str_replace($invalid_characters, "", $string);
  return $string;
}