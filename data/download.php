<?php
setlocale(LC_ALL,"es_ES");
define('SITE_ROOT', dirname(__FILE__));
header('Content-Type: application/json; charset=utf-8');

ini_set('max_execution_time', 30000);

//copy('https://docs.google.com/spreadsheets/d/12r_i6nuQSNvSP43UX0mpUuAYdZTYfuNznC10W8y5B-Y/export?gid=0&format=csv', './downs/routing.csv');
copy('https://docs.google.com/spreadsheets/d/1M9-51AkDDz4069xIcOnMYvU047O6q2lZVMuWtK75Hl0/export?gid=0&format=csv', './downs/deuda.csv');
copy('https://docs.google.com/spreadsheets/d/1MjuurNhe101b6VUpX0BA_U_bJ1zqHDRZ-43PhJBBCDc/export?gid=402575387&format=csv', './downs/obras.csv');
copy('https://docs.google.com/spreadsheets/d/1idRfvKOjuNdnrVLIwAD9g8RLmRh1_oc94GtpL5LpF8U/export?gid=318142443&format=csv', './downs/valencia.csv');
copy('https://docs.google.com/spreadsheets/d/1Z5EOh2uxVb0tg22TdRchTmyYkdNtC_RrtJ9YIpjsux4/export?gid=0&format=csv', './downs/refs.csv');
copy('https://docs.google.com/spreadsheets/d/1LosCtg2nzfR1H6sECoBPT4HVkdW7BCgD2zbUvvvHzGA/export?gid=0&format=csv', './downs/deudaResumen.csv');

writeJson('./downs/deuda.json', readCSV('./downs/deuda.csv'));


function writeJson($fileName, $array) {
  $out = fopen($fileName, 'w');
  fwrite($out, utf8_encode(json_encode($array)));
  fclose($out);
}


function readCSV($fileName) {

  $outp = array();
  $count = 0;

  if (($gestor = fopen($fileName, "r")) !== FALSE) {
    while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
      if($count == 0) {
        $count++;
        continue;
      }
      array_push($outp, $datos);
    }
    fclose($gestor);
  }
  
  return $outp;
}